# GNU Astro Distortion Converion

This repository contains the test files before merging them to GnuAstro.

**/scripts** contains python scripts to be used for some automation and calculations.

**/test-fits** contains fits files used to test the functions of the library.

`temp-wcs.c` is the main file containing all the code. Modularity will be implemented later;-).